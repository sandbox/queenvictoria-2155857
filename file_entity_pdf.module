<?php

  /* 
   * Implements hook_file_default_types
   */
  function file_entity_pdf_file_default_types() {
    $types = array();

  // PDF.
    $types['pdf'] = (object) array(
      'api_version' => 1,
      'type' => 'pdf',
      'label' => t('PDF'),
      'description' => t('A <em>PDF</em> file is a Portable Document.'),
      'mimetypes' => array(
        'application/pdf',
      ),
      'export_module' => 'file_entity_pdf',
      'disabled' => FALSE,
    );
    return $types;
  }

  function file_entity_pdf_file_default_types_alter(&$types) {
    // remove application/pdf from the list
    $types['document']->mimetypes = $types['document']->mimetypes;

    $types = array_merge($types, file_entity_pdf_file_default_types());
  }
  /*
   * Create the required fields and widgets
   * One field_pdf_dummy with the pdf_to_image widget.
   * One field_pdf_images with imagefield.
   */
  function _file_entity_pdf_create_dummy_fields() {
    $t = get_t();

    // Create the pdf image field and instance.
    // Define the pdf image field.
    $pdf_image_field = array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_file_pdf_images',
      'module' => 'image',
      'settings' => array(
        // multiple
      ),
      'translatable' => '0',
      'type' => 'image',
    );

    // As long as the pdf dummy field doesn't already exist create it.
    if (!field_info_field($pdf_image_field['field_name'])) {
      field_create_field($pdf_image_field);
    }

    // Define the pdf dummy instance.
    $pdf_image_instance = array(
      'bundle' => 'pdf',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => $t('This field is used to store the images created of the PDF.'),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'full' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'file',
      'field_name' => 'field_file_pdf_images',
      'label' => 'PDF images',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'image',
        'weight' => '-4',
      ),
    );

    // As long as the pdf image instance doesn't already exist create it.
    if (!field_info_instance($pdf_image_instance['entity_type'], $pdf_image_instance['field_name'], $pdf_image_instance['bundle'])) {
      field_create_instance($pdf_image_instance);
    }

    // Define the pdf dummy field.
    $pdf_dummy_field = array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_file_pdf_dummy',
      'module' => 'pdf_to_image',
      'settings' => array(
      ),
      'translatable' => '0',
      'type' => 'file',
    );

    // As long as the pdf dummy field doesn't already exist create it.
    if (!field_info_field($pdf_dummy_field['field_name'])) {
      field_create_field($pdf_dummy_field);
    }

    // Define the pdf dummy instance.
    $pdf_dummy_instance = array(
      'bundle' => 'pdf',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => $t('This field is used to store a second dummy reference to the PDF file itself.'),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'full' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'file',
      'field_name' => 'field_file_pdf_dummy',
      'label' => 'PDF dummy',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'pdf_to_image',
        'settings' => array(
          'pdf_to_image' => array(
            'target_field'        => 'field_file_pdf_images',
            'density'             => '72x72',
            'extra_args'          => '',
          ),
        ),
        'type' => 'pdf_to_image',
        'weight' => '-4',
      ),
    );

    // As long as the alt text instance doesn't already exist create it.
    if (!field_info_instance($pdf_dummy_instance['entity_type'], $pdf_dummy_instance['field_name'], $pdf_dummy_instance['bundle'])) {
      field_create_instance($pdf_dummy_instance);
    }
  }

  /*
   * Implements hook_install
   */
  function file_entity_pdf_install() {
    _file_entity_pdf_create_dummy_fields();
  }

  /*
   * Implements hook_file_presave
   * On save copy the file FID into the field_pdf_dummy field
   */
  function file_entity_pdf_file_presave($file) {
    if ( property_exists($file, 'field_file_pdf_dummy') ) {
      // of course its probably empty at first
      error_log(print_r($file, TRUE));
      if ( ! count($file->field_file_pdf_dummy[LANGUAGE_NONE]) ) {
        $new = file_unmanaged_copy($file->uri, $file->uri, FILE_EXISTS_RENAME);

        $newfile            = new stdClass();
        $newfile->filename  = $file->filename;
        $newfile->uri       = $new;
        // $newfile->filemime  = $file->filemime;
        // $newfile->filesize  = $file->filesize;
        // $newfile->uid       = $file->uid;
        // $newfile->fid       = NULL;
        // $newfile->timestamp = $file->timestamp;
        $newfile = file_save($newfile);
        $newfile->display = 1;
        $file->field_file_pdf_dummy = array(LANGUAGE_NONE => array('0' => (array)$newfile));
      } else {
        foreach ( $file->field_file_pdf_dummy[LANGUAGE_NONE][0] as $key => $value ) {
          // if we copy the FID too then we get recursion in the db
          if ( property_exists($file, $key) &&  $file->$key != $value && $key != 'fid' ) {
  //          error_log("====================== overwriting $key");
  //          error_log(print_r($file->{$key}, TRUE));
  //          error_log(print_r($value, TRUE));
            $file->field_file_pdf_dummy[LANGUAGE_NONE][0][$key] = $file->$key;
          }
        }
      }
    }
  }

  /*
   * Possibly move the presave function into this module 
   * (as its' not required otherwise)
   */

